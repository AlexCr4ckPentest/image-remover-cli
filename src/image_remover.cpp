#include "image_remover.hpp"

Image::Image()
  : width_ {0}
  , height_ {0}
  , channels_count_ {0}
  , data_ {nullptr}
{ }

Image::~Image()
{
  clear();
}

void Image::load_from_file(std::string_view filename)
{
  clear();
  data_ = stbi_load(filename.data(), &width_, &height_, &channels_count_, STBI_default);
  if (!data_)
  {
    clear();
    throw std::runtime_error {"Failed to load image: " + std::string{stbi_failure_reason()}};
  }
}

std::pair<int, int> Image::get_size() const noexcept
{
  return {width_, height_};
}

int Image::get_channels() const noexcept
{
  return channels_count_;
}

void Image::clear() noexcept
{
  stbi_image_free(data_);
  data_ = nullptr;
}



ImageRemover::ImageRemover() noexcept
  : max_image_width_{0}
  , max_image_height_{0}
{ }

ImageRemover::ImageRemover(int max_image_width, int max_image_height) noexcept
  : max_image_width_{max_image_width}
  , max_image_height_{max_image_height}
{ }

void ImageRemover::start_removing(const std::vector<std::filesystem::path>& images_to_remove)
{
  if (!images_to_remove.empty())
  {
    Image image {};
    for (const auto& filename : images_to_remove)
    {
      image.load_from_file(filename.string());
      if (need_to_remove(image))
      {
        std::filesystem::remove(filename);
      }
    }
  }
}

void ImageRemover::set_max_image_width(int max_image_width) noexcept
{
  max_image_width_ = max_image_width;
}

void ImageRemover::set_max_image_height(int max_image_height) noexcept
{
  max_image_height_ = max_image_height;
}

bool ImageRemover::need_to_remove(const Image& img) noexcept
{
  const auto [image_width, image_height] {img.get_size()};
  return (image_width < max_image_width_ && image_height < max_image_height_);
}
