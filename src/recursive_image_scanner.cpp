#include "recursive_image_scanner.hpp"



RecursiveImageScanner::RecursiveImageScanner()
  : initial_path_{}
  , detected_images_ {}
  , image_file_extensions_ {".png", ".jpg", ".jpeg"}
{ }



RecursiveImageScanner::RecursiveImageScanner(std::string_view initial_path)
  : initial_path_{initial_path}
  , detected_images_ {}
  , image_file_extensions_ {".png", ".jpg", ".jpeg"}
{ }



void RecursiveImageScanner::set_initial_path(std::string_view initial_path)
{
  initial_path_ = initial_path;
}



void RecursiveImageScanner::add_image_file_extension(std::string_view file_extension)
{
  if (std::find(image_file_extensions_.begin(), image_file_extensions_.end(), file_extension)
      == image_file_extensions_.end())
  {
    image_file_extensions_.emplace_back(file_extension);
  }
}



void RecursiveImageScanner::start_scan(scan_finished_callback scan_finished_callback)
{
  if (!std::filesystem::exists(initial_path_))
  {
    throw std::runtime_error {"No such directory: " + initial_path_};
  }

  std::filesystem::recursive_directory_iterator r_it{initial_path_};
  for (; r_it != std::filesystem::recursive_directory_iterator{}; ++r_it)
  {
    if (!r_it->is_directory() && is_image(r_it->path().extension()))
    {
      detected_images_.emplace_back(*r_it);
    }
  }

  scan_finished_callback(detected_images_);
}



bool RecursiveImageScanner::is_image(const std::string& ext)
{
  return std::find(image_file_extensions_.begin(), image_file_extensions_.end(), ext)
      != image_file_extensions_.end();
}
