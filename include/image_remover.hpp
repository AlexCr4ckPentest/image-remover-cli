#pragma once

#include <vector>
#include <filesystem>

#include "stb/stb_image.h"

class Image final
{
public:
  Image();
  ~Image();

  void load_from_file(std::string_view filename);

  std::pair<int, int> get_size() const noexcept;
  int get_channels() const noexcept;

private:
  int width_;
  int height_;
  int channels_count_;
  unsigned char* data_;

  void clear() noexcept;
};



class ImageRemover final
{
public:
  ImageRemover(const ImageRemover&) = delete;
  ImageRemover(ImageRemover&&) = delete;
  ImageRemover& operator=(const ImageRemover&) = delete;
  ImageRemover& operator=(ImageRemover&&) = delete;

  ImageRemover() noexcept;
  ImageRemover(int max_image_width, int max_image_height) noexcept;

  void start_removing(const std::vector<std::filesystem::path>& images_to_remove);

  int max_image_width() const noexcept;
  int max_image_height() const noexcept;
  void set_max_image_width(int max_image_width) noexcept;
  void set_max_image_height(int max_image_height) noexcept;

private:
  int max_image_width_;
  int max_image_height_;
  bool need_to_remove(const Image& img) noexcept;
};
