#pragma once

#include <string>
#include <vector>
#include <filesystem>
#include <functional>

class RecursiveImageScanner final
{
private:
  using scan_finished_callback = std::function<void(std::vector<std::filesystem::path>&)>;

public:
  RecursiveImageScanner();
  RecursiveImageScanner(std::string_view initial_path);

  RecursiveImageScanner(const RecursiveImageScanner&) = delete;
  RecursiveImageScanner(RecursiveImageScanner&&) = delete;
  RecursiveImageScanner& operator=(const RecursiveImageScanner&) = delete;
  RecursiveImageScanner& operator=(RecursiveImageScanner&&) = delete;

  void set_initial_path(std::string_view initial_path);
  void add_image_file_extension(std::string_view file_extension);
  void start_scan(scan_finished_callback scan_finished_callback);

private:
  std::string initial_path_;
  std::vector<std::filesystem::path> detected_images_;
  std::vector<std::string> image_file_extensions_;

  bool is_image(const std::string& ext);
};
