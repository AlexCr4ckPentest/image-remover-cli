#include <iostream>
#include <string>
#include <utility>

#include "recursive_image_scanner.hpp"
#include "image_remover.hpp"

int main(int argc, char** argv)
{
  if (argc < 3 || std::string_view{argv[1]} == "--help")
  {
    std::cout << "Usage: " << argv[0] << " <path> <size>\n"
              << "---------------------------------------\n"
              << "Example: " <<  argv[0] << " /home/Pictures 1920x1080\n"
              << "remove all images in dir '/home/Pictures' with resulution lower than 1920x1080\n";
    return 1;
  }

  const std::string_view initial_path {argv[1]};
  const std::string_view max_image_resolution {argv[2]};
  const auto delim_position {max_image_resolution.find('x')};

  if (delim_position == std::string::npos)
  {
    std::cout << argv[0] << ": error: invalid resolution format, see help\n";
    return 2;
  }

  const auto width {max_image_resolution.substr(0, delim_position)};
  const auto height {max_image_resolution.substr(delim_position + 1)};
  RecursiveImageScanner recursive_image_scanner {};
  ImageRemover image_remover {};

  recursive_image_scanner.set_initial_path(initial_path);
  image_remover.set_max_image_width(std::stoull(width.begin()));
  image_remover.set_max_image_height(std::stoull(height.begin()));

  try
  {
    recursive_image_scanner.start_scan(
          [&image_remover](std::vector<std::filesystem::path>& images_to_remove)
    {
        image_remover.start_removing(images_to_remove);
    });
  }
  catch (const std::runtime_error& err)
  {
    std::cout << argv[0] << ": " << err.what() << "\n";
    return 1;
  }

  return 0;
}
